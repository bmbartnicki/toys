# Toys

## Co to git i Gitlab?
Git - system konroli wersji, możemy w nim zapisywać konkretnę wersję naszego kodu i do niej wrócić.
repozytorium gita jest przechowywane na Twoim komputerze, musisz je świadomie wypchnąć (push) na serwer
Będziemy używać darmowego serwera git rozszerzonego o ładny webowy interfejs gitlab.com (jest to konkurnacja dla githuba)

## Praca z gitem
Wszystko co masz poniżej można wyklikać w pycharmie
```
git clone http://....... - służy do pierwszego sklonowania repozytorium na Twój komputerze
git add nazwa_pliku mówisz gitowie, że ten plik ma zacząć być śledzony.
git commit -m "komentarz" - powoduje że "zdjęcie" wszystkich dodanych do śledzenia plików zostaje zapisane 
git push - powoduje wysłanie Twoich lokalnych zmiań na serwer
git pull - powoduje ściągnięcie z serwera nowych zmian.
```

Generalnie praca powinna wyglądać tak:
1) Otwierasz pycharma
1) git pull aby mieć pewność że pracujesz na najnowszej wersji kodu
1) robisz kilka git commotów
1) git pull - ktoś w między czasie mógł dokonać zmian w edytowanych przez Ciebie plikach
1) W przypadku "konfliktów"  rozwiązujesz je - brzmi dziwnie, ale tak naprawdę to jest kilka kliknięć w pycharmie
1) git push - wypychasz zmiany na serwe

## Django
Django - jeden z najpopularniejszych pythonowych framworków do robienia aplikacji webowych
Realizuje o wzorzeć Model View Controller - tak naprawdę sprowadz się do to tego, że mamy w ważne pliki
model.py - zawiera opis danych (tabel w bazie danych)
view.py - zawiera opis widoków, upraszczając podstron naszej aplikacji
controller jest realizowany przez sam framwork

### Cwiczenia 1
1) Instalacja Django
```pip install django```
W jakiej wersji masz django?
Dobrą praktyką jest zapisywanie wersji używanych bibliotek w pliku requirements.txt.
W każdje lini pliku piszesz nazwę biblioteki==wersja
2) Stwórz plik requirements.txt i go uzupełnij
3) zcommituj i wypchnij zmiany
4) Stwórz nowy projekt django, w tym celu wydaj: ```django-admin``` i 
zastanów się jakiej komendy użyć do stworznia projektu 
4) wejdź to stworzonego projektu i wydaj python manage.py, zapoznaj się z opcjami
5) Stwórz nową aplikację (nazwa)
6) Odpal serwer
7) zobacz czy działa w przeglądarce
8) wpisz dowolny ciąg w urlu po / - mamy tryb developerski wieć widzimy dostępne urle
9) wejdz w panel administracyjny i spróbuj się zalogować
10) Coś poszło nie tak, czy widzisz co?
11) Naprawiamy: ```makemigrations; migrate```
12) Próbujemy się zalogować drugi raz :)
13) Stwórz superusera
14) Wreszcie logowanie powinno się udać :)
15) commit&push

# ćwiczenie 2
Zaprojektujemy aplikacje
1) Baza danych 
2) Mockupy 

# ćwiczenie 3*
Przeglądnijmy pliki w projekcie, aby następnym razem wiedzieć co gidze jest

# Co będziemy robić następnym razem
(kolejność może ulec zmianie)
- zaprojektowaną bazę danych "opiszemy" w model.py
- porozmawiamy dokładniej o migracjach
- django-admin
- zapoznamy się z Boostrapem 
- stworzymy szablon/szablony stron
- wyciągniemy dane z modelu
- użyjemy danych do wyświetlenia modelu
- edycja, dodawanie, usuwanie danych + szablony
- pep8 i prosty CI w gitlabie
- Przejrzymy projekt, czy jest coś co będzie wymagało nowej wiedzy
